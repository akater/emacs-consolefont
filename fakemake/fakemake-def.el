;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'consolefont
  authors "Dima Akater"
  first-publication-year-as-string "2023"
  org-files-in-order '("consolefont-core"
                       "consolefont")
  site-lisp-config-prefix "50"
  license "GPL-3")
